package com.redmadrobot.config

/**
  * Config defaults
  */
object Defaults {
  /** Default configuration host */
  val CONFIG_HOST = "localhost"
  /** Default configuration port */
  val CONFIG_PORT = 8080
  /** Default rabbit mq hosts */
  val CONFIG_MQ_HOSTS = ""
  /** Default kafka broker string */
  val CONFIG_KAFKA_BROKER = ""
}
