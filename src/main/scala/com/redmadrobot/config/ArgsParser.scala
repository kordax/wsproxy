package com.redmadrobot.config

import com.redmadrobot.chatlib.common.Constants
import org.log4s.getLogger

/**
  * Application arguments parser
  */
class ArgsParser {
  private val LOGGER = getLogger
  /** Invalid characters for argument value */
  private val HOST_INVALID_CHARS = Array("|/\\.!@#$%^&*()_+=-§±?><}{[]:;'\"~`".toCharArray)

  /** Current config */
  private var config = None: Option[Config]

  /** Current parser implementation */
  private val parser = new scopt.OptionParser[Config]("""sbt run""") {
    head(Constants.WSPROXY_APP_NAME, Constants.WSPROXY_VERSION)

    opt[String]('h', "host").action((x, c) => c.copy(host = x)).text(s"host or ip addr to bind to (DEFAULT: ${Defaults.CONFIG_HOST})")
    opt[Int]('p', "port").action((x, c) => c.copy(port = x)).text(s"port to bind to (DEFAULT: ${Defaults.CONFIG_PORT})")
    opt[String]('z', "mq").action((x, c) => {
      LOGGER.debug("Handling host string: " + x)
      var hostStrSpl = x.split(":")
      // Create new HostEntry instance from splitted string, where element at 0 index is actual host and another one
      // is a port

      LOGGER.debug(s"Parsed port: ${hostStrSpl(1).toInt}")

      var username = ""
      var password = ""

      // If number of arguments is higher than 2, then username is provided
      if (hostStrSpl.length > 2) {
        username = hostStrSpl(2)
        LOGGER.debug(s"Parsed username: $username")

        // If number of arguments is higher than 3, then password is provided
        if (hostStrSpl.length > 3) {
          password = hostStrSpl(3)
          LOGGER.debug(s"Parsed password: $password")
        }
      }

      c.copy(rabbit = HostEntry(hostStrSpl(0), hostStrSpl(1).toInt, username, password))
    }).text("list of remote RabbitMQ hosts " +
            s"""(including port number and optional username + password divided by '_'), divided by colon, for example: "localhost:5672,192.168.1.2:5672:user:password"\n
               |(DEFAULT: localhost:${Defaults.CONFIG_HOST})
             """.stripMargin)

    opt[String]('k', "kafka").action((x, c) => c.copy(kafka = x)).text(s"single kafka broker (DEFAULT: ${Defaults.CONFIG_KAFKA_BROKER})")
    help("help").text(s"""EXAMPLE: sbt "run -h 192.168.1.1 -p 8080 -z ${Defaults.CONFIG_MQ_HOSTS} -k ${Defaults.CONFIG_KAFKA_BROKER}" """)
    version("version")
  }

  /**
    * Parses given arguments into internal config
    *
    * @param args arguments
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def parse(args: Array[String]): Unit = {
    LOGGER.debug("Parsing application arguments: " + args)
    parser.parse(args, Config()) match {
      case Some(parsedConf) =>
        config = Some(parsedConf)
        LOGGER.debug("Args passed: " + parsedConf)
      case None => throw new IllegalArgumentException("Passed arguments are illegal")
    }
  }

  /**
    * Validates arguments
    *
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def validateArgs(): Unit = {
    LOGGER.debug("Validating application arguments: " + config)
    validateHost(config.get.host)
    validatePort(config.get.port)
    validateMQHost(config.get.rabbit)
  }

  /**
    * Get current config instance
    *
    * @return
    */
  def getConfig: Config = {
    LOGGER.debug("Getting config")
    config.get
  }

  /**
    * Validates host
    *
    * @param host host
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validateHost(host: String): Unit = {
    LOGGER.debug("Validating host: " + host)
    if (host.toCharArray.contains(HOST_INVALID_CHARS)) {
      println("host argument contains invalid characters")
      throw new IllegalArgumentException("host argument contains invalid characters")
    }

    for (c <- host.toCharArray) {
      if (c > 127) {
        LOGGER.error("host argument contains invalid characters")
        throw new IllegalArgumentException("host argument contains invalid characters")
      }
    }
  }

  /**
    * Validates port
    *
    * @param port port
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validatePort(port: Int): Unit = {
    LOGGER.debug("Validating port: " + port)
    if (port < 1 || port > 65535) {
      LOGGER.error("port argument isn't valid")
      throw new IllegalArgumentException("port argument isn't valid")
    }
  }

  /**
    * Validates remote MQ host
    *
    * @param entry MQ host entry
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validateMQHost(entry: HostEntry): Unit = {
    if (!entry.host.isEmpty && entry.port > 0) {
      LOGGER.debug("Validating MQ host: " + entry)
      validateHost(entry.host)
      validatePort(entry.port)
    }
  }
}
