package com.redmadrobot.config

/**
  * Configuration case class
  *
  * @param host host
  * @param port port
  * @param rabbit remote MQ hosts
  */
case class Config(host: String = Defaults.CONFIG_HOST,
                  port: Int = Defaults.CONFIG_PORT,
                  rabbit: HostEntry = HostEntry("", -1, "", ""),
                  kafka: String = Defaults.CONFIG_KAFKA_BROKER)