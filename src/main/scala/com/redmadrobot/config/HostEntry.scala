package com.redmadrobot.config

case class HostEntry(host: String = "",
                     port: Int = 80,
                     username: String,
                     password: String)