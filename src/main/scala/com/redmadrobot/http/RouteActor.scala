package com.redmadrobot.http

import akka.http.scaladsl.model.ws.Message
import akka.stream.actor.ActorPublisher
import org.log4s.getLogger

class RouteActor extends ActorPublisher[Message] {
  private val LOGGER = getLogger
  override def receive: Receive = {
    case msg: Message =>
      while (totalDemand > 0) {
        onNext(msg)
      }
  }

  override def postStop(): Unit = {
    LOGGER.debug("PostStop")
  }
}