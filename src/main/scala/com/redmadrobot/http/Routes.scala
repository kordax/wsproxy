package com.redmadrobot.http

import java.util.concurrent.atomic.AtomicLong

import akka.actor.{ActorSystem, Props}
import akka.event.Logging
import akka.http.scaladsl.model.ws.{BinaryMessage, Message}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream._
import akka.stream.actor.ActorPublisher
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.redmadrobot.chatlib.common.Constants
import com.redmadrobot.chatlib.logging.LoggingHelpers
import com.redmadrobot.protobuf.{HandlerManager, PBHandler}
import org.log4s.getLogger

import scala.concurrent.ExecutionContext

/**
  * Basic object that contains implemented AKKA routes for HTTP server singleton
  *
  */
object Routes {
  private val LOGGER = getLogger
  implicit val system: ActorSystem = ActorSystem(Logging.simpleName(this).replaceAll("\\$", ""))
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val ec: ExecutionContext = ExecutionContext.Implicits.global
  /** URL Suffix for new message request */
  private val GET_USERS_SUFFIX = "GetUsers"
  /** URL Suffix for new message request */
  private val CREATE_MSG_SUFFIX = "CreateMessage"
  /** URL Suffix for new room request */
  private val CREATE_ROOM_SUFFIX = "CreateRoom"
  /** URL Suffix for new room request */
  private val SEND_MSG_SUFFIX = "SendMessage"

  var numOfClients: AtomicLong = new AtomicLong(0L)

  private var routes: Option[Route] = None

  var binHandler: PBHandler = _

  /**
    * Get routes
    *
    * @return
    */
  def getRoutes: Route = {
    binHandler = HandlerManager.getHandler
    if (routes.isDefined) {
      routes.get
    } else {
      routes = Some(webSocketRoute ~ getBasicRoute)
      routes.get
    }
  }

  /**
    * Basic route for GET request without URL suffix
    */
  private val getBasicRoute: Route = get {
    pathEndOrSingleSlash {
      complete("Welcome to WS Proxy HTTP Server version: " + Constants.WSPROXY_VERSION)
    }
  }

  /**
    * Basic route for GET request without URL suffix
    */
  private val getUsersRoute: Route = get {
    pathSuffix(CREATE_MSG_SUFFIX) {
      complete("Welcome to WS Proxy HTTP Server version: " + Constants.WSPROXY_VERSION)
    }
  }

  /**
    * Route for new chat group PUT request
    */
  private val putCreateMessageRoute: Route = post {
    pathSuffix(CREATE_MSG_SUFFIX) {
      complete("Group successfully created")
    }
  }

  /**
    * Route for new room PUT request
    */
  private val putCreateRoomRoute: Route = post {
    pathSuffix(CREATE_ROOM_SUFFIX) {
      complete("Room successfully created")
    }
  }

  /**
    * Route for new message POST request
    */
  private val postSendMessageRoute: Route = post {
    pathSuffix(SEND_MSG_SUFFIX) {
      complete("Message was sent")
    }
  }

  /**
    * Route for WebSocket request
    */
  private val webSocketRoute: Route = pathSuffix(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX) {
    handleWebSocketMessages(wsRouteFlow)
  }

  /**
    * This method calls all registered handlers of [[com.redmadrobot.protobuf.PBHandler]] type.
    * This method also using round-robin simple balancing by comparing adapters of each handler pack.\
    * This is done by calling a pack of handlers for a "next" adapter and saving last called adapter into a variable
    *
    * @return flow for handleWebSocketMessages method
    */
  private def wsRouteFlow: Flow[Message, Message, Any] = {
    val routeActorRef = system.actorOf(Props[RouteActor])
    val routeActor = ActorPublisher[Message](routeActorRef)

    val flow = Flow.fromSinkAndSource(Sink.foreach[Message] {
      case bm: BinaryMessage =>
        // We handle the same message with different handlers
        try {
          binHandler.handle(bm, routeActorRef)
        } catch {
          case default: Exception => LOGGER.error(LoggingHelpers.getStackTraceAsString(default))
        }
      case _ =>
    }, Source.fromPublisher(routeActor))

    flow
  }
}
