package com.redmadrobot

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.redmadrobot.chatlib.adapters.kafka.{KafkaAdapter, PBMessageDeserializer, PBMessageSerializer}
import com.redmadrobot.chatlib.protobuf.messages.PBMessage
import com.redmadrobot.config.{ArgsParser, Config}
import com.redmadrobot.http.Routes
import com.redmadrobot.protobuf.{HandlerManager, KafkaHandler}
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.log4s.getLogger

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Main application object
  */
object Main extends App {
  private val LOGGER = getLogger
  final val argsParser = new ArgsParser

  implicit val system: ActorSystem = ActorSystem("akka-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  LOGGER.info("Starting WSProxy")

  val config = parseArgs()
  registerProtoBufHandlers(30000)

  val binding = Http().bindAndHandle(Routes.getRoutes, config.host, config.port)
  LOGGER.info(s"WSProxy is now online at http://${config.host}:${config.port}")

  sys.addShutdownHook({
    binding.flatMap(_.unbind()).onComplete(_ => system.terminate())
    LOGGER.info("Shutting down WSProxy")
  })

  /**
    * Parse application arguments
    */
  private def parseArgs(): Config = {
    LOGGER.info(s"Arguments size: ${args.length}")
    argsParser.parse(args)
    val config = argsParser.getConfig
    argsParser.validateArgs()

    config
  }

  /**
    * Register ProtoBuf handlers for all MQ adapters
    *
    * @param timeoutMS timeout in milliseconds
    */
  private def registerProtoBufHandlers(timeoutMS: Int): Unit = {
    // Register Kafka handler
    val kafkaAdapter = new KafkaAdapter[String, PBMessage](
      config.kafka,
      "wsproxy",
      keySerializer = new StringSerializer,
      valueSerializer = new PBMessageSerializer,
      keyDeserializer = new StringDeserializer,
      valueDeserializer = new PBMessageDeserializer
    )

    HandlerManager.registerHandler(new KafkaHandler(kafkaAdapter))
  }
}