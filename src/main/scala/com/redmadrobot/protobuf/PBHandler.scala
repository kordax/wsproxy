package com.redmadrobot.protobuf

import akka.actor.ActorRef
import akka.http.scaladsl.model.ws.BinaryMessage
import com.redmadrobot.chatlib.adapters.Adapter
import com.redmadrobot.chatlib.adapters.mq.RabbitMQAdapter

/**
  * Basic ProtoBuf handler trait
  */
abstract class PBHandler(adapter: Adapter) {
  /**
    * Main binary message handler method
    *
    * @param message message
    * @param actorRef socket actor reference
    * @throws java.lang.Exception if any error occurred
    * @return response
    */
  @throws(classOf[RuntimeException])
  def handle(message: BinaryMessage, actorRef: ActorRef): Unit

  /**
    * Get handler adapter
    *
    * @return [[RabbitMQAdapter]] instance
    */
  def getAdapter: Adapter = {
    adapter
  }

  override def toString = s"PBHandler(${getAdapter.getConnectionString})"
}