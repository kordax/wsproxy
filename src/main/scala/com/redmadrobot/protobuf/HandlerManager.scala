package com.redmadrobot.protobuf

import org.log4s.getLogger

import scala.collection.mutable.ArrayBuffer

object HandlerManager {
  private val LOGGER = getLogger
  private var handler: PBHandler = _

  /**
    * Registers [[PBHandler]] for [[akka.http.scaladsl.model.ws.BinaryMessage]] handling
    *
    * @param handler [[PBHandler]] instance
    */
  def registerHandler(handler: PBHandler): Unit = {
    LOGGER.debug("Registering binary handler: " + handler.toString)
    this.handler = handler
  }

  /**
    * Get valid handler
    *
    * @return [[ArrayBuffer]] of [[PBHandler]] instances
    */
  def getHandler: PBHandler = {
    LOGGER.debug("Retrieving registered binary handlers: ")
    handler
  }
}
