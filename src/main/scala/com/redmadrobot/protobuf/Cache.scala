package com.redmadrobot.protobuf

import com.redmadrobot.chatlib.protobuf.messages._
import org.log4s.getLogger

import scala.collection.mutable

/**
  * Main cache class that's capable of caching and retrieving cached messages.
  * Cache map key consists of session ID and [[MessageType]] enumeration, while values are
  * * tuples of last request timestamp in milliseconds and response message reference.
  *
  */
object Cache extends mutable.HashMap[(String, Int), (Long, PBMessage)] {
  private val LOGGER = getLogger

  private val cacheLifeTimeMS = 60000
  private val cacheManagerHeartBeatMS = 5000

  private val cacheManagerT = new Thread(new CacheManager())
  // TODO: Thread state exceptions not handled
  cacheManagerT.start()

  /**
    * Put message to cache and remember it's timestamp
    *
    * @param topic user topic
    * @param taskType task type (STATUS, AUTH... etc.)
    * @param message message to cache
    * @throws IllegalArgumentException if message argument is invalid
    * @throws RuntimeException if message argument is invalid
    * @return cached response message instance reference
    */
  @throws(classOf[IllegalArgumentException])
  @throws(classOf[RuntimeException])
  def put(topic: String, taskType: Int, message: PBMessage): PBMessage = {
    // Message for put method should be of RESPONSE type
    if (message.`type`.equals(MessageType.TASK)) {
      throw new IllegalArgumentException(s"Invalid message was sent to cache, message is of TASK type: ${message.getTask.`type`}, $topic")
    }

    try {
      put((topic, taskType), (System.currentTimeMillis(), message))
      LOGGER.debug(s"Cached response for session id: $topic")
    } catch {
      case e: Throwable =>
        throw new RuntimeException(s"Failed to push message to cache: ${e.getMessage}")
    }

    message
  }

  /**
    * Check if there's cached response present in this cache for this task message and return result of [[Option]] type
    *
    * @param sessionID session id
    * @param message task message of [[TaskMessage]] type
    * @return [[Option]] of type [[Some]] tuple with timestamp and [[ResponseMessage]] instance reference if
    *        entry exists or [[Null]] if there's no cached response for this task message
    */
  def ask(sessionID: String, message: TaskMessage): Option[(Long, PBMessage)] = {
    LOGGER.debug(s"Retrieving cached response for session id: $sessionID")
    get((sessionID, message.`type`.index))
  }

  /**
    * Watchdog class that monitors cache lifetime
    *
    */
  private class CacheManager extends Runnable {
    override def run(): Unit = {
      while (true) {
        for (sessionEntry <- toIterable) {
          val cacheEntry = sessionEntry._2
          if ((System.currentTimeMillis() - cacheEntry._1) > cacheLifeTimeMS) {
            LOGGER.warn(s"Removing entry from cache: ${sessionEntry._1}")
            remove(sessionEntry._1)
          }
        }

        Thread.sleep(cacheManagerHeartBeatMS)
      }
    }
  }
}
