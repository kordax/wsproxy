package com.redmadrobot.protobuf

import java.io._
import java.util
import java.util.regex.Pattern

import akka.actor.ActorRef
import akka.http.scaladsl.model.ws.BinaryMessage
import akka.util.ByteString
import com.redmadrobot.chatlib.adapters.kafka.KafkaAdapter
import com.redmadrobot.chatlib.common.Crypto
import com.redmadrobot.chatlib.logging.LoggingHelpers
import com.redmadrobot.chatlib.protobuf.messages.ResponseErrorCode._
import com.redmadrobot.chatlib.protobuf.messages.ResponseMessage.{ResponseAuthMessage, ResponseErrorMessage}
import com.redmadrobot.chatlib.protobuf.messages._
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.kafka.common.TopicPartition
import org.log4s.getLogger

import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Main [[akka.http.scaladsl.model.ws.BinaryMessage]] handler
  * When new instance is created, provided MQ adapter will initiate connection
  */
class KafkaHandler(adapter: KafkaAdapter[String, PBMessage]) extends PBHandler(adapter) {
  private val LOGGER = getLogger
  private val sessions = new scala.collection.mutable.HashMap[String, mutable.Set[ActorRef]] with mutable.MultiMap[String, ActorRef]
  private val producer: KafkaProducer[String, PBMessage] = adapter.getProducer
  private val consumer: KafkaConsumer[String, PBMessage] = adapter.getConsumer

  val USER_TOPICS = s"${KafkaAdapter.KAFKA_USER_TOPIC_PREFIX}.*"
  LOGGER.debug(s"Subscribing to $USER_TOPICS")
  consumer.subscribe(Pattern.compile(USER_TOPICS))

  val consumerThread: Thread = new Thread() {
    override def run(): Unit = {
      while (true) {
        try {
          val it = consumer.poll(30000).iterator()

          while (it.hasNext) {
            val record = it.next
            val request: PBMessage = record.value
            LOGGER.debug(s"Consumed message from topic: ${record.topic}")

            for (session <- sessions(record.topic.substring(KafkaAdapter.KAFKA_USER_TOPIC_PREFIX.length))) {
              session ! akka.http.scaladsl.model.ws.BinaryMessage(ByteString(request.toByteArray))
            }
          }
        } catch {
          case e: Exception => LOGGER.error(s"Exception: ${LoggingHelpers.getStackTraceAsString(e)}")
        }
      }
    }
  }

  LOGGER.info("Starting consumer thread")
  consumerThread.start()

  /**
    * Main handler method
    *
    * @param message message
    * @param actorRef actor reference
    * @throws java.lang.Exception if any error occurred
    */
  @throws(classOf[RuntimeException])
  override def handle(message: BinaryMessage, actorRef: ActorRef): Unit = {
    var clientName = ""

    try {
      var request = PBMessage.parseFrom(message.getStrictData.toArray)

      clientName = request.clientName
      LOGGER.debug(s"Received request with client name: $clientName")

      if (clientName.isEmpty) {
        actorRef ! BinaryMessage(ByteString(PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), clientName).withResponse(
          ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_ERR_NOT_AUTH, Some("No client name provided")))
        ).toByteArray))
        return
      }

      // Handle authorization
      if (request.getTask.`type` == TaskMessageType.TASK_AUTH) {
        try {
          // Subscribe to response queue
          var authMsg = handleAuth(request)

          if (!clientName.isEmpty) {
            LOGGER.info(s"Authorized client with name: $clientName")
            sessions.addBinding(clientName, actorRef)
          }

          val userConsumer = adapter.makeConsumer()
          val topicPartition = new TopicPartition(KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, 0)
          val partitions = new util.ArrayList[TopicPartition]()
          partitions.add(topicPartition)

          userConsumer.assign(partitions)

          val latestOffset = userConsumer.endOffsets(partitions).get(topicPartition)
          LOGGER.debug(s"Received auth request with offset ${request.getTask.getAuthMsg.offset}, latest offset: $latestOffset")
          if (request.getTask.getAuthMsg.offset < latestOffset) {
            userConsumer.poll(0)
            userConsumer.seek(topicPartition, request.getTask.getAuthMsg.offset)

            val it = userConsumer.poll(60000).iterator()

            while (it.hasNext) {
              val record = it.next()
              if (record.value.getResponse.`type` != ResponseMessageType.RESP_AUTH) {
                LOGGER.info(s"Synchronizing request for client '$clientName'")
                actorRef ! akka.http.scaladsl.model.ws.BinaryMessage(ByteString(record.value.toByteArray))
              }
            }

            authMsg = authMsg.withResponse(authMsg.getResponse.withAuthMsg(authMsg.getResponse.getAuthMsg.withOffset(Predef.Long2long(latestOffset))))
          }

          actorRef ! BinaryMessage(ByteString(authMsg.toByteArray))
          return
        } catch {
          case e: IOException =>
            LOGGER.error(s"Error on handling authorization: ${e.getMessage}")
            LOGGER.debug(s"Current registered sessions:")
            sessions.keys.foreach(x => LOGGER.debug(x))

            actorRef ! BinaryMessage(ByteString(PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), clientName).withResponse(
              ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_ERR_NOT_AUTH, Some(s"Not authorized: ${e.getMessage}")))
            ).toByteArray))
            return
        }
      }

      try {
        producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_TASKS_TOPIC, request))
      } catch {
        case default: Exception =>
          LOGGER.error(s"Failed to publish message: ${LoggingHelpers.getStackTraceAsString(default)}")

          val errMsg = PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), clientName).withResponse(
            ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_ERR_UNKNOWN, Some(s"Binary handler failed to handle message for unknown reasons: ${default.getMessage}, ${default.getCause}")))
          )

          actorRef ! BinaryMessage(ByteString(errMsg.toByteArray))
      }
    } catch {
      case default: Exception =>
        LOGGER.error(s"Unknown exception: ${LoggingHelpers.getStackTraceAsString(default)}")
        val errMsg = PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), clientName).withResponse(
          ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_ERR_UNKNOWN, Some(s"Binary handler failed to handle message for unknown reasons: ${default.getMessage}, ${default.getCause}")))
        )

        actorRef ! BinaryMessage(ByteString(errMsg.toByteArray))
    }
  }

  /**
    * Handle authorization packet and return response with new session id
    *
    * @param request incoming request
    * @throws java.io.IOException on error
    * @return authorization response
    */
  @throws(classOf[IOException])
  def handleAuth(request: PBMessage): PBMessage = {
    LOGGER.debug("Handling authorization")

    if (request.`type` == MessageType.TASK) {
      if (request.getTask.`type` == TaskMessageType.TASK_AUTH) {
        val sessID = Crypto.generateSessionID(request.toByteArray)

        val response = PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), sessID).withResponse(
          ResponseMessage(ResponseMessageType.RESP_AUTH).withAuthMsg(ResponseAuthMessage(sessID, 0L))
        )

        response
      } else {
        throw new IOException("Not an authorization packet!")
      }
    } else {
      throw new IOException("Not an authorization packet!")
    }
  }
}
