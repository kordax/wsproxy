package com.redmadrobot.tests

import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.{AtomicInteger, AtomicLong}

import akka.actor.ActorSystem
import akka.http.scaladsl.model.ws.BinaryMessage
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.ByteString
import com.redmadrobot.chatlib.common.Constants
import com.redmadrobot.chatlib.protobuf.messages.TaskMessage.{TaskCreateRoomMessage, TaskPostMessage}
import com.redmadrobot.chatlib.protobuf.messages._
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.tests.wsclient.WebSocketClient
import org.log4s.{Logger, getLogger}
import org.scalatest._

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.collection.parallel.ForkJoinTaskSupport
import scala.collection.parallel.mutable.ParArray
import scala.concurrent.{ExecutionContext, TimeoutException}
import scala.util.control.Breaks._

/**
  * Load tests with status messages that are supposed to be cached
  */
class BenchmarkTestPostMsgs extends FlatSpec with Matchers with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  private implicit val LOGGER: Logger = getLogger
  implicit val executionContext: ExecutionContext = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wsclient-akka-system")
  implicit val materializer: Materializer = ActorMaterializer()

  final val argsParser = new ArgsParser
  var config: Config = _
  val connectTimeout: AtomicInteger = new AtomicInteger(30000)
  var wsClient: WebSocketClient = _

  private val df = new SimpleDateFormat("HH:MM:s")
  private var tStart = 0L

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }

        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)
    wsClient = new WebSocketClient(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, config.host, config.port)

    connectTimeout.set(config.connTout)
  }

  /**
    * Parse application arguments
    */
  private def parseArgs(args: Array[String]): Config = {
    argsParser.parse(args)
    val config = argsParser.getConfig
    argsParser.validateArgs()

    config
  }

  before {
    tStart = System.currentTimeMillis()
    LOGGER.info(s"Load test started at: ${df.format(new Date(tStart))}")
  }

  after {
    LOGGER.info(s"Load test started at: ${df.format(new Date(tStart))}")
    LOGGER.info(s"Load test finished at: ${df.format(new Date(System.currentTimeMillis()))}")
    LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
  }

  s"Multiple clients " should s" load the server with status tasks" in {
    val clients = ParArray.fill[WebSocketClient](config.clients)(new WebSocketClient(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, config.host, config.port))
    var requests: AtomicLong = new AtomicLong(0L)

    clients.tasksupport = new ForkJoinTaskSupport(new scala.concurrent.forkjoin.ForkJoinPool(Runtime.getRuntime.availableProcessors * 2))
    Thread.sleep(1000)

    try {
      clients.foreach { client =>
        client.connect(connectTimeout.get)
      }
    } catch {
      case default: Throwable => fail(default)
    }

    try {
      var i: AtomicInteger = new AtomicInteger(100)
      clients.foreach { client =>
        this.synchronized {
          i.incrementAndGet()
          client.authorize(i.toString, config.connTout)
        }
      }
    } catch {
      case e: TimeoutException => fail(e)
    }

    var responseTimes: ListBuffer[Long] = new ListBuffer()
    var testTimeNs = TimeUnit.SECONDS.toNanos(config.time)

    val createRoomRequest = PBMessage(MessageType.TASK, System.currentTimeMillis(), wsClient.getClientName)
      .withTask(TaskMessage(TaskMessageType.TASK_CREATE_ROOM)
        .withCreateRoomMsg(TaskCreateRoomMessage("test_group_chat", `private` = true)))

    wsClient.sendToQueue(ByteString(createRoomRequest.toByteArray))
    val response = wsClient.awaitResponse(config.connTout)
    if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
      fail(s"Error message received: ${response.getResponse.getErrorMsg.code}:${response.getResponse.getErrorMsg.text.get}")
    } else {
      LOGGER.info(s"Created test room with room id: ${response.getResponse.getStatusMsg.data.get}")
    }

    val task = TaskMessage(TaskMessageType.TASK_POST).withPostMsg(TaskPostMessage(response.getResponse.getStatusMsg.data.get, "BenchmarkMessage"))
    val prepRequest = PBMessage(MessageType.TASK, System.currentTimeMillis(), wsClient.getClientName).withTask(task)
    val asyncRequests: mutable.HashMap[String, BinaryMessage] = new mutable.HashMap[String, BinaryMessage]()

    LOGGER.info(s"Example request structure: ${PBMessage(MessageType.TASK, System.currentTimeMillis(), wsClient.getClientName).withTask(task).toProtoString}")
    LOGGER.info(s"Example request size: ${prepRequest.toByteArray.length} bytes")

    clients.foreach { client =>
      this.synchronized {
        LOGGER.info(s"Preparing request for client: ${client.getClientName}")
        asyncRequests.put(client.getClientName, BinaryMessage(ByteString(PBMessage(MessageType.TASK, System.currentTimeMillis(), client.getClientName).withTask(task).toByteArray)))
      }
    }

    LOGGER.info(s"Sending messages for next ${config.time} seconds")

    var testStart = System.nanoTime()
    while ((System.nanoTime() - testStart) < testTimeNs) {
      clients.foreach { client =>
        var t0 = System.nanoTime()

        client.sendToQueue(asyncRequests(client.getClientName))
        val response = client.awaitResponse(config.connTout)
        LOGGER.debug(s"Received request [type: ${response.`type`}, client_name: ${response.clientName}, code: ${response.getResponse.getStatusMsg.code}, text: ${response.getResponse.getStatusMsg.text}]")

        if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
          LOGGER.error(s"Received error [client_name: ${response.clientName}, code: ${response.getResponse.getErrorMsg.code}, text: ${response.getResponse.getErrorMsg.text.get}]")
          fail(s"Received error [client_name: ${response.clientName}, code: ${response.getResponse.getErrorMsg.code}, text: ${response.getResponse.getErrorMsg.text.get}]")
        }

        requests.getAndIncrement()

        val responseTime = System.nanoTime() - t0

        this.synchronized {
          responseTimes += responseTime
        }

        LOGGER.debug("Elapsed time: " + responseTime + " ns")
      }
    }
    var endTime = System.nanoTime()

    try {
      for (client <- clients) {
        client.disconnect(30)
      }
    } catch {
      case e: IOException => fail(e)
    }

    var medRespTime = 0L
    val sortResponseTimes = responseTimes.sortWith(_ < _)

    if (sortResponseTimes.size % 2 == 1) {
      medRespTime = sortResponseTimes(sortResponseTimes.size / 2)
    } else {
      val (up, down) = sortResponseTimes.splitAt(sortResponseTimes.size / 2)
      medRespTime = (up.last + down.head) / 2
    }

    var testTimeSecs = (endTime - testStart) / 1000000000.0D

    LOGGER.info(s"Total time elapsed on request and response: $testTimeSecs seconds")
    LOGGER.info(s"Median response time: $medRespTime nanoseconds, or ${TimeUnit.NANOSECONDS.toMillis(medRespTime)} milliseconds, or ${medRespTime / 1000000000.0D} seconds")
    LOGGER.info(s"Requests per second: ${requests.get() / testTimeSecs}")

    succeed
  }
}