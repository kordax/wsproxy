package com.redmadrobot.tests

import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.ByteString
import com.redmadrobot.chatlib.common.Constants
import com.redmadrobot.chatlib.protobuf.messages.TaskMessage.TaskStatusMessage
import com.redmadrobot.chatlib.protobuf.messages._
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.tests.wsclient.WebSocketClient
import org.log4s.{Logger, getLogger}
import org.scalatest._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext
import scala.util.control.Breaks._

/**
  * Main load tests
  * class
  */
class BenchmarkAuthOffsetTest extends FlatSpec with Matchers with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  private implicit val LOGGER: Logger = getLogger
  implicit val executionContext: ExecutionContext = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wsclient-akka-system")
  implicit val materializer: Materializer = ActorMaterializer()

  final val argsParser = new ArgsParser
  var config: Config = _
  val connectTimeout: AtomicInteger = new AtomicInteger(30000)
  var wsClient: WebSocketClient = _

  private val df = new SimpleDateFormat("HH:MM:ss")
  private var tStart = 0L

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }

        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)
    wsClient = new WebSocketClient(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, config.host, config.port)

    connectTimeout.set(config.connTout)
  }

  /**
    * Parse application arguments
    */
  private def parseArgs(args: Array[String]): Config = {
    argsParser.parse(args)
    val config = argsParser.getConfig
    argsParser.validateArgs()

    config
  }

  before {
    tStart = System.currentTimeMillis()
    LOGGER.info(s"Load test started at: ${df.format(new Date(tStart))}")
  }

  after {
    LOGGER.info(s"Load test started at: ${df.format(new Date(tStart))}")
    LOGGER.info(s"Load test finished at: ${df.format(new Date(System.currentTimeMillis()))}")
    LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
  }

  s"Second client with same client name " should " receive whole diff from kafka" in {
    wsClient.connect(connectTimeout.get)
    wsClient.authorize("1", config.connTout)

    val desyncNumber = 100000

    val request: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), "1").withTask(
      TaskMessage(TaskMessageType.TASK_STATUS).withStatusMsg(TaskStatusMessage())
    )

    // Successfully authorized, now send 1000 messages and receive them on the next client

    for (i <- 0 until desyncNumber) {
      wsClient.sendToQueue(ByteString(request.toByteArray))
    }

    val other = new WebSocketClient(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, config.host, config.port)
    other.connect(config.connTout)

    LOGGER.info("Starting synchronization")
    val testStart = System.nanoTime()
    other.authorize("1", config.connTout)
    // Wait for all 100k responses + 1 authorization response
    val responses = other.awaitResponses(desyncNumber + 1, config.connTout)
    val endTime = System.nanoTime()

    for (response <- responses) {
      if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
        fail(s"Error message received: ${response.getResponse.getErrorMsg.code}:${response.getResponse.getErrorMsg.text.get}")
      }
    }

    val testTimeSecs = (endTime - testStart) / 1000000000.0D

    LOGGER.info(s"Total time elapsed on synchronization: $testTimeSecs seconds")
    LOGGER.info(s"Responses per second: ${desyncNumber / testTimeSecs}")
    LOGGER.info(s"Responses total: $desyncNumber")

    succeed
  }
}