package com.redmadrobot.tests.config

import com.redmadrobot.chatlib.common.Constants
import org.log4s.getLogger

/**
  * Tests arguments parser
  */
class ArgsParser {
  private val LOGGER = getLogger
  /** Invalid characters for argument value */
  private val HOST_INVALID_CHARS = Array("|/\\.!@#$%^&*()_+=-§±?><}{[]:;'\"~`".toCharArray)

  /** Current config */
  private var config = None: Option[Config]

  /** Current parser implementation */
  private val parser = new scopt.OptionParser[Config](Constants.WSPROXY_APP_NAME) {
    head("""EXAMPLE: sbt testOnly * -- -Dh=10.110.10.207 -Dp=8080 -Dt=60 -Dc=120000 -Dd=500000 -Dn=500""")

    opt[String]('h', "host").action((x, c) => c.copy(host = x)).text("host or ip addr to bind to")
    opt[Int]('p', "port").action((x, c) => c.copy(port = x)).text("port to bind to")
    opt[Int]('c', "conn").action((x, c) => c.copy(connTout = x)).text("connection timeout in milliseconds")
    opt[Int]('t', "time").action((x, c) => c.copy(time = x)).text("load test time in seconds")
    opt[Int]('n', "clients").action((x, c) => c.copy(clients = x)).text("number of clients in load test")
    opt[Int]('d', "delay").action((x, c) => c.copy(delay = x)).text("delay between requests in milliseconds")


    help("help").text("""EXAMPLE: sbt "testOnly * -- -Dh=10.110.10.207 -Dp=8080 -Dt=60 -Dc=120000 -Dd=15 -Dn=500" """)
    version(Constants.WSPROXY_VERSION)
  }

  /**
    * Parses given arguments into internal config
    *
    * @param args arguments
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def parse(args: Array[String]): Unit = {
    LOGGER.info("Parsing application arguments: " + args.toIterable.toSeq.toString())
    parser.parse(args, Config()) match {
      case Some(parsedConf) =>
        config = Some(parsedConf)
        LOGGER.debug("Args passed: " + parsedConf)
      case None => throw new IllegalArgumentException("Passed arguments are illegal")
    }
  }

  /**
    * Validates arguments
    *
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def validateArgs(): Unit = {
    LOGGER.debug("Validating application arguments: " + config)
    validateHost(config.get.host)
    validatePort(config.get.port)
  }

  /**
    * Get current config instance
    *
    * @return
    */
  def getConfig: Config = {
    LOGGER.debug("Getting config")
    config.get
  }

  /**
    * Validates host
    *
    * @param host host
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validateHost(host: String): Unit = {
    LOGGER.debug("Validating host: " + host)
    if (host.toCharArray.contains(HOST_INVALID_CHARS)) {
      println("host argument contains invalid characters")
      throw new IllegalArgumentException("host argument contains invalid characters")
    }

    for (c <- host.toCharArray) {
      if (c > 127) {
        LOGGER.error("host argument contains invalid characters")
        throw new IllegalArgumentException("host argument contains invalid characters")
      }
    }
  }

  /**
    * Validates port
    *
    * @param port port
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validatePort(port: Int): Unit = {
    LOGGER.debug("Validating port: " + port)
    if (port < 1 || port > 65535) {
      LOGGER.error("port argument isn't valid")
      throw new IllegalArgumentException("port argument isn't valid")
    }
  }
}
