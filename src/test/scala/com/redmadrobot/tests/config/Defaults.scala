package com.redmadrobot.tests.config

object Defaults {
  /** Default configuration host */
  val CONFIG_HOST = "localhost"
  /** Default configuration port */
  val CONFIG_PORT = 8080
  /** Default configuration load test time in seconds */
  val CONFIG_LOAD_TIME_S = 60
  /** Default configuration load test time in milliseconds */
  val CONFIG_CONN_TIMEOUT_MS = 30000
  /** Default configuration load test number of clients */
  val CONFIG_NUM_OF_CLIENTS = 100
  /** Default configuration request delay in nanoseconds */
  val CONFIG_REQ_DELAY_MS = 0
}