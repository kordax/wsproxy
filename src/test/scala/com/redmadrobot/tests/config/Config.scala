package com.redmadrobot.tests.config

/**
  *
  * @param host host
  * @param port port
  * @param time load test time
  * @param connTout connection timeout
  * @param clients number of clients
  * @param delay request delay
  */
case class Config(host: String = Defaults.CONFIG_HOST,
                  port: Int = Defaults.CONFIG_PORT,
                  time: Int = Defaults.CONFIG_LOAD_TIME_S,
                  connTout: Int = Defaults.CONFIG_CONN_TIMEOUT_MS,
                  clients: Int = Defaults.CONFIG_NUM_OF_CLIENTS,
                  delay: Int = Defaults.CONFIG_REQ_DELAY_MS)