package com.redmadrobot.tests.wsclient

import java.io.IOException
import java.text.SimpleDateFormat
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.atomic.AtomicInteger

import akka.Done
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.ws.{BinaryMessage, Message, WebSocketRequest}
import akka.http.scaladsl.settings.ClientConnectionSettings
import akka.stream.scaladsl.{Keep, Sink, Source, SourceQueue}
import akka.stream.{Materializer, OverflowStrategy}
import akka.util.ByteString
import com.redmadrobot.chatlib.common.Constants
import com.redmadrobot.chatlib.protobuf.messages.TaskMessage.TaskAuthMessage
import com.redmadrobot.chatlib.protobuf.messages.{MessageType, PBMessage, TaskMessage, TaskMessageType}
import org.log4s.Logger

import scala.collection.mutable.ArrayBuffer
import scala.concurrent._

/**
  * AKKA-based WebSocket client wrapper
  *
  * @param suffix ws or wss protocol
  * @param host host
  * @param port port
  */
class WebSocketClient(suffix: String = Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, host: String, port: Int)(implicit executionContext: ExecutionContext = ExecutionContext.global, system: ActorSystem, materializer: Materializer, LOGGER: Logger) {
  private var queue: SourceQueue[Message] = _

  private var connected: Boolean = false
  private var responses: ConcurrentLinkedQueue[PBMessage] = new ConcurrentLinkedQueue[PBMessage]
  private var clientName = ""
  private var offset: Long = 0L

  val pingCounter = new AtomicInteger()

  /**
    * Initiate WebSocket connection
    *
    * @param timeoutMS timeout in milliseconds
    * @throws java.io.IOException if connection has failed
    * @return
    */
  @throws(classOf[IOException])
  def connect(timeoutMS: Long): Unit = {
    LOGGER.info(s"Connecting to ws://$host:$port/$suffix")

    if (connected) {
      LOGGER.info("Already connected")

      return
    }

    val t0 = System.currentTimeMillis()

    val defSettings = ClientConnectionSettings(system)
    val customWebsocketSettings = defSettings.websocketSettings.withPeriodicKeepAliveData(() ⇒ ByteString(s"d${pingCounter.incrementAndGet()}"))
    val customSettings = defSettings.withWebsocketSettings(customWebsocketSettings)

    var wsFlow = Http().webSocketClientFlow(WebSocketRequest(s"ws://$host:$port/$suffix"), Http().defaultClientHttpsContext, None, customSettings, system.log)
    connected = false

    var flow = wsFlow.mapMaterializedValue(upgradeResponse => upgradeResponse.flatMap {
      upgrade =>
        if (upgrade.response.status == StatusCodes.SwitchingProtocols) {
          LOGGER.debug(s"Upgrade response status: ${upgrade.response.status}")
          LOGGER.info(s"Connection to ws://$host:$port/$suffix successfully established")
          connected = true
          Future.successful(Done)
        } else {
          LOGGER.error(s"Connection failed with status: ${upgrade.response.status}")
          throw new IOException(s"Connection failed with status: ${upgrade.response.status}")
        }
    })

    queue = Source.queue[Message](1000000, OverflowStrategy.fail)
      .via(flow).toMat(Sink.foreach[Message](msg => {
      responses.add(PBMessage.parseFrom(msg.asBinaryMessage.getStrictData.toArray))
    }))(Keep.left).run()

    while (!connected) {
      if ((System.currentTimeMillis() - t0) > timeoutMS) {
        throw new TimeoutException("Timeout error while waiting for connection")
      }

      Thread.sleep(500)
    }
  }

  /**
    * Disconnect
    *
    * @param timeout timeout in seconds
    */
  def disconnect(timeout: Int): Unit = {
    val completion = queue.watchCompletion()
    completion.onComplete(_ => LOGGER.info("WebSocket client successfully disconnected"))

    this.connected = false
    this.responses.clear()
  }

  /**
    * Send bytes to WebSocket flow
    *
    * @param bytes bytes to send
    * @throws java.lang.RuntimeException if connection error occurred
    */
  def sendToQueue(bytes: ByteString): Unit = {
    LOGGER.debug(s"Sending message to ws://$host:$port/$suffix")
    queue offer BinaryMessage(bytes)
  }

  /**
    * Send BinaryMessage to WebSocket flow (higher performance method)
    *
    * @param message bytes to send
    * @throws java.lang.RuntimeException if connection error occurred
    */
  def sendToQueue(message: BinaryMessage): Unit = {
    LOGGER.debug(s"Sending message to ws://$host:$port/$suffix")
    queue offer message
  }

  /**
    * Send authorization request and return session id
    *
    * @param clientName client name
    * @param timeoutMS response timeout
    * @param offset last user log offset
    * @throws IOException if timeout has occurred
    * @throws TimeoutException if timeout has occurred
    */
  @throws(classOf[IOException])
  @throws(classOf[TimeoutException])
  def authorize(clientName: String, timeoutMS: Int, offset: Long = 0L): Unit = {
    LOGGER.info(s"Authorizing client $clientName")
    if (!connected) {
      throw new IOException("Failed to authorize, no connection")
    }

    this.clientName = clientName
    var request: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), clientName).withTask(
      TaskMessage(TaskMessageType.TASK_AUTH).withAuthMsg(TaskAuthMessage(offset))
    )

    sendToQueue(ByteString(request.toByteArray))

    try {
      val response = awaitResponse(timeoutMS)
      this.offset = response.getResponse.getAuthMsg.offset
      LOGGER.info(s"Successfully authorized client: $clientName")
    } catch {
      case e: TimeoutException => throw e
    }
  }

  /**
    * Get client name
    *
    * @return client name
    */
  def getClientName: String = {
    clientName
  }

  /**
    * Get current log offset
    *
    * @return offset
    */
  def getOffset: Long = {
    offset
  }

  /**
    * Await for some response
    * If already contains some responses, a list of responses will be returned immediately
    *
    * @param timeoutMS timeout in milliseconds
    * @throws TimeoutException if timeout has occurred
    * @return response
   */
  @throws(classOf[TimeoutException])
  def awaitResponse(timeoutMS: Int): PBMessage = {
    LOGGER.debug(s"Awaiting for response [client: $clientName]")
    val t0 = System.currentTimeMillis()

    while (responses.isEmpty) {
      if ((System.currentTimeMillis() - t0) > timeoutMS) {
        throw new TimeoutException("Timeout reached while waiting for response")
      }
    }

    responses.poll()
  }

  /**
    * Await for responses until timeout occurs
    *
    * @param waitTime timeout in milliseconds
    * @return list of responses
    */
  def awaitResponses(waitTime: Int): ArrayBuffer[PBMessage] = {
    LOGGER.debug(s"Awaiting for responses for next $waitTime milliseconds [client: $clientName]")
    val results: ArrayBuffer[PBMessage] = new ArrayBuffer[PBMessage](0)

    while (true) {
      try {
        results += awaitResponse(waitTime)
      } catch {
        case _: Throwable => return results
      }
    }

    results
  }

  /**
    * Await for specific number of responses
    *
    * @param responseNum number of responses to wait
    * @param waitTime timeout in milliseconds
    * @return list of responses
    */
  def awaitResponses(responseNum: Int, waitTime: Int): ArrayBuffer[PBMessage] = {
    LOGGER.debug(s"Awaiting for responses for next $waitTime milliseconds [client: $clientName]")
    val results: ArrayBuffer[PBMessage] = new ArrayBuffer[PBMessage](0)

    while (results.length < responseNum) {
      try {
        results += awaitResponse(waitTime)
        LOGGER.debug("Received response")
      } catch {
        case _: Throwable => return results
      }
    }

    results
  }
}

object WebSocketClient {
  private val df = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss")
}
