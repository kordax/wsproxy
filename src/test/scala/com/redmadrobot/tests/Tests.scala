package com.redmadrobot.tests

import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.concurrent.atomic.AtomicInteger

import akka.actor.ActorSystem
import akka.stream.{ActorMaterializer, Materializer}
import akka.util.ByteString
import com.redmadrobot.chatlib.common.Constants
import com.redmadrobot.chatlib.protobuf.messages.TaskMessage.{TaskCreateRoomMessage, TaskPostMessage}
import com.redmadrobot.chatlib.protobuf.messages._
import com.redmadrobot.tests.config.{ArgsParser, Config}
import com.redmadrobot.tests.wsclient.WebSocketClient
import org.log4s.{Logger, getLogger}
import org.scalatest._

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.{ExecutionContext, TimeoutException}
import scala.util.control.Breaks._

/**
  * Main tests class
  */
class Tests extends FlatSpec with Matchers with BeforeAndAfter with BeforeAndAfterAllConfigMap {
  private implicit val LOGGER: Logger = getLogger
  implicit val executionContext: ExecutionContext = ExecutionContext.global
  implicit val system: ActorSystem = ActorSystem("wsclient-akka-system")
  implicit val materializer: Materializer = ActorMaterializer()

  final val argsParser = new ArgsParser
  var config: Config = _
  val connectTimeout: AtomicInteger = new AtomicInteger(30000)
  var wsClient: WebSocketClient = _

  private val df = new SimpleDateFormat("HH:MM:s")
  private var tStart = 0L

  override def beforeAll(configMap: ConfigMap): Unit = {
    var argsArr = ArrayBuffer[String]()

    breakable {
      for (entry <- configMap.toArray) {
        if (entry._1.contains("help")) {
          argsArr += "--help"
          break()
        }
        
        argsArr += "-" + entry._1
        argsArr += entry._2.toString
      }
    }

    config = parseArgs(argsArr.toArray)
    wsClient = new WebSocketClient(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, config.host, config.port)

    connectTimeout.set(config.connTout)
  }

  /**
    * Parse application arguments
    */
  private def parseArgs(args: Array[String]): Config = {
    argsParser.parse(args)
    val config = argsParser.getConfig
    argsParser.validateArgs()

    config
  }

  before {
    tStart = System.currentTimeMillis()
    LOGGER.info(s"Test started at: ${df.format(new Date(tStart))}")
  }

  after {
    LOGGER.info(s"Test started at: ${df.format(new Date(tStart))}")
    LOGGER.info(s"Test finished at: ${df.format(new Date())}")
    LOGGER.info(s"Overall elapsed time: ${System.currentTimeMillis() - tStart} milliseconds")
  }

//  "WebSocket client" should "connect and receive status" in {
//    try {
//      wsClient.connect(connectTimeout.get)
//      wsClient.authorize("1", config.connTout)
//
//      val request: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), "1").withTask(
//        TaskMessage(TaskMessageType.TASK_STATUS).withStatusMsg(TaskStatusMessage())
//      )
//
//      val t0 = System.nanoTime()
//      wsClient.sendToQueue(ByteString(request.toByteArray))
//      val response = wsClient.awaitResponse(config.connTout)
//      if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
//        fail(s"Error message received: ${response.getResponse.getErrorMsg.code}:${response.getResponse.getErrorMsg.text.get}")
//      }
//
//      LOGGER.info(s"Received request [type: ${response.getResponse.`type`.name}, client_name: ${response.clientName}, code: ${response.getResponse.getStatusMsg.code}, text: ${response.getResponse.getStatusMsg.text}]")
//      LOGGER.info("Elapsed time: " + (System.nanoTime() - t0) + " ns")
//    } catch {
//      case e: IOException => fail(e)
//      case e: TimeoutException => fail(e)
//    } finally {
//      wsClient.disconnect(config.connTout)
//    }
//
//    succeed
//  }
//
//  "WebSocket client" should "connect and post a message" in {
//    try {
//      wsClient.connect(connectTimeout.get)
//      wsClient.authorize("1", config.connTout)
//
//      val request: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), "1").withTask(
//        TaskMessage(TaskMessageType.TASK_POST).withPostMsg(TaskPostMessage(1, "test message"))
//      )
//
//      val t0 = System.nanoTime()
//      wsClient.sendToQueue(ByteString(request.toByteArray))
//      val response = wsClient.awaitResponse(config.connTout)
//      if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
//        fail(s"Error message received: ${response.getResponse.getErrorMsg.code}:${response.getResponse.getErrorMsg.text.get}")
//      }
//
//      LOGGER.info(s"Received request [type: ${response.`type`}, client_name: ${response.clientName}, code: ${response.getResponse.getStatusMsg.code}, text: ${response.getResponse.getStatusMsg.text}]")
//      LOGGER.info("Elapsed time: " + (System.nanoTime() - t0) + " ns")
//    } catch {
//      case e: IOException => fail(e)
//      case e: TimeoutException => fail(e)
//    } finally {
//      wsClient.disconnect(config.connTout)
//    }
//
//    succeed
//  }
//
//  "WebSocket client" should "connect and create new group chat" in {
//    try {
//      wsClient.connect(connectTimeout.get)
//      wsClient.authorize("1", config.connTout)
//
//      val request: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), "1").withTask(
//        TaskMessage(TaskMessageType.TASK_CREATE_ROOM).withCreateRoomMsg(TaskCreateRoomMessage("test_group_chat", `private` = true))
//      )
//
//      val t0 = System.nanoTime()
//      wsClient.sendToQueue(ByteString(request.toByteArray))
//      val response = wsClient.awaitResponse(config.connTout)
//      if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
//        fail(s"Error message received: ${response.getResponse.getErrorMsg.code}:${response.getResponse.getErrorMsg.text.get}")
//      }
//
//      LOGGER.info(s"Received request [type: ${response.`type`}, client_name: ${response.clientName}, code: ${response.getResponse.getStatusMsg.code}, text: ${response.getResponse.getStatusMsg.text}]")
//      LOGGER.info("Elapsed time: " + (System.nanoTime() - t0) + " ns")
//    } catch {
//      case e: IOException => fail(e)
//      case e: TimeoutException => fail(e)
//    } finally {
//      wsClient.disconnect(config.connTout)
//    }
//
//    succeed
//  }
//
//  "3 WebSocket clients" should "create a room, join it and receive a message posted by master client" in {
//    try {
//      val clients = Array.fill[WebSocketClient](3)(new WebSocketClient(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, config.host, config.port))
//      wsClient.connect(connectTimeout.get)
//
//      try {
//        clients.foreach { client =>
//          client.connect(connectTimeout.get)
//        }
//      } catch {
//        case default: Throwable => fail(default)
//      }
//
//      Thread.sleep(1000)
//      wsClient.authorize("1", config.connTout)
//
//      // Authorize rest of the clients
//      try {
//        var i: AtomicInteger = new AtomicInteger(100)
//        clients.foreach { client =>
//          this.synchronized {
//            i.incrementAndGet()
//            client.authorize(i.toString, config.connTout)
//          }
//        }
//      } catch {
//        case e: TimeoutException => fail(e)
//      }
//
//      val request: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), "1").withTask(
//        TaskMessage(TaskMessageType.TASK_CREATE_ROOM).withCreateRoomMsg(TaskCreateRoomMessage("test_group_chat", `private` = true))
//      )
//
//      val t0 = System.nanoTime()
//      wsClient.sendToQueue(ByteString(request.toByteArray))
//      val response = wsClient.awaitResponse(config.connTout)
//      if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
//        fail(s"Error message received: ${response.getResponse.getErrorMsg.code}:${response.getResponse.getErrorMsg.text.get}")
//      }
//
//      val roomID: AtomicLong = new AtomicLong(response.getResponse.getStatusMsg.data.get)
//
//      LOGGER.info(s"Registered room id: $roomID")
//
//      LOGGER.info(s"Received request [type: ${response.`type`}, client_name: ${response.clientName}, code: ${response.getResponse.getStatusMsg.code}, text: ${response.getResponse.getStatusMsg.text}]")
//      LOGGER.info("Elapsed time: " + (System.nanoTime() - t0) + " ns")
//
//      clients.foreach { client =>
//        var t0 = System.nanoTime()
//
//        if (config.delay > 0) {
//          Thread.sleep(config.delay)
//        }
//
//        client.sendToQueue(
//          ByteString(PBMessage(MessageType.TASK, System.currentTimeMillis(), client.getClientName).withTask(
//            TaskMessage(TaskMessageType.TASK_JOIN_ROOM).withJoinRoomMsg(
//              TaskJoinRoomMessage(roomID.get()))).toByteArray)
//        )
//
//        val response = client.awaitResponse(config.connTout)
//        LOGGER.info(s"Room client received join on room response [type: ${response.`type`}, client_name: ${response.clientName}, code: ${response.getResponse.getStatusMsg.code}, text: ${response.getResponse.getStatusMsg.text}]")
//
//        if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
//          LOGGER.error(s"Received error [client_name: ${response.clientName}, code: ${response.getResponse.getErrorMsg.code}, text: ${response.getResponse.getErrorMsg.text.get}]")
//          fail(s"Received error [client_name: ${response.clientName}, code: ${response.getResponse.getErrorMsg.code}, text: ${response.getResponse.getErrorMsg.text.get}]")
//        }
//
//        LOGGER.debug(s"Elapsed time: ${System.nanoTime() - t0} ns")
//      }
//
//      val postRequest: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), "1").withTask(
//        TaskMessage(TaskMessageType.TASK_POST).withPostMsg(TaskPostMessage(roomID.get(), "Test message we all should see!"))
//      )
//
//      val pt0 = System.nanoTime()
//      wsClient.sendToQueue(ByteString(postRequest.toByteArray))
//      val postResponses = wsClient.awaitResponse(config.connTout)
//
//      clients.foreach { client =>
//        val response = client.awaitResponse(config.connTout)
//        LOGGER.info(s"Room client received response with post [type: ${response.`type`}, client_name: ${response.clientName}, code: ${response.getResponse.getStatusMsg.code}, text: ${response.getResponse.getStatusMsg.text}]")
//        LOGGER.info("Elapsed time: " + (System.nanoTime() - pt0) + " ns")
//      }
//
//      clients.foreach { client =>
//        LOGGER.info("Leaving a room")
//        client.sendToQueue(
//          ByteString(PBMessage(MessageType.TASK, System.currentTimeMillis(), client.getClientName).withTask(
//            TaskMessage(TaskMessageType.TASK_LEAVE_ROOM).withLeaveRoomMsg(
//              TaskLeaveRoomMessage(roomID.get()))).toByteArray)
//        )
//
//        client.awaitResponse(config.connTout)
//      }
//    } catch {
//      case e: IOException => fail(e)
//      case et: TimeoutException => fail(et)
//      case default: Throwable => fail(default)
//    } finally {
//      wsClient.disconnect(config.connTout)
//    }
//
//    succeed
//  }

  "Two WebSocket clients with the same client name" should "synchronize to their state" in {
    try {
      val other = new WebSocketClient(Constants.WSPROXY_WEBSOCKET_PATH_SUFFIX, config.host, config.port)

      wsClient.connect(connectTimeout.get)
      other.connect(connectTimeout.get)

      wsClient.authorize("sameClientName", config.connTout)

      // Now send some requests and make sure that other client will receive them on synchronization

      val request: PBMessage = PBMessage(MessageType.TASK, System.currentTimeMillis(), "sameClientName").withTask(
        TaskMessage(TaskMessageType.TASK_CREATE_ROOM).withCreateRoomMsg(TaskCreateRoomMessage("test_group_chat", `private` = true))
      )

      wsClient.sendToQueue(ByteString(request.toByteArray))
      val newRoomResponse = wsClient.awaitResponse(config.connTout)
      if (newRoomResponse.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
        fail(s"Error message received: ${newRoomResponse.getResponse.getErrorMsg.code}:${newRoomResponse.getResponse.getErrorMsg.text.get}")
      }

      LOGGER.debug(s"Response received: ${newRoomResponse.getResponse.getStatusMsg.text}")

      val postMsgRequest = PBMessage(MessageType.TASK, System.currentTimeMillis(), "sameClientName").withTask(
          TaskMessage(TaskMessageType.TASK_POST).withPostMsg(TaskPostMessage(newRoomResponse.getResponse.getStatusMsg.data.get, "test message"))
      )

      wsClient.sendToQueue(ByteString(postMsgRequest.toByteArray))
      val postMsgResponse = wsClient.awaitResponse(config.connTout)
      if (postMsgResponse.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
        fail(s"Error message received: ${postMsgResponse.getResponse.getErrorMsg.code}:${postMsgResponse.getResponse.getErrorMsg.text.get}")
      }

      LOGGER.debug(s"Response received: ${postMsgResponse.getResponse.getStatusMsg.text}")

      other.authorize("sameClientName", config.connTout)
      val responses = other.awaitResponses(10000)

      for (response <- responses) {
        if (response.getResponse.`type`.equals(ResponseMessageType.RESP_ERROR)) {
          fail(s"Error message received: ${response.getResponse.getErrorMsg.code}:${response.getResponse.getErrorMsg.text.get}")
        }
        LOGGER.info(s"Received response: ${response.`type`.toString()}")
      }
    } catch {
      case e: IOException => fail(e)
      case e: TimeoutException => fail(e)
    } finally {
      wsClient.disconnect(config.connTout)
    }

    succeed
  }
}